Compare Offer
============

> *Dey compared er jurbs - [http://youtu.be/768h3Tz4Qik](http://youtu.be/768h3Tz4Qik)*

**Gabino Dabdoub, Tim Martin, Trey Moore, Steven Wojcio**

CS 4911, Design Capstone, Spring 2014

Note: The developer documentation can be found in the docs/ directory of our
source.

Production Site
---------------
The site may be viewed live at:
UPDATE: This production site is currently down, our database grew too large with
the location and cost of living data for the Heroku free tier.

### [`compareoffer.herokuapp.com`](http://compareoffer.herokuapp.com/)


Getting Started
---------------

Here's what you'll need to do to serve or start hacking on our application on your machine.

This guide assumes you are working on a Unix-like environment with programs and libraries traditionally used for building and developing software available. If you are using OS X, ensure you've installed XCode (and accepted it's license agreement, otherwise it won't work).

If you run into issues using this guide, let us know!

1. [Install Python 3.3.3](https://bitbucket.org/team7seniordesign/compareoffer/overview/#markdown-header-install-python-333)
2. [Install setuptools](https://bitbucket.org/team7seniordesign/compareoffer/overview/#markdown-header-install-setuptools)
3. [Install pip](https://bitbucket.org/team7seniordesign/compareoffer/overview/#markdown-header-install-pip)
4. [Install virtualenv](https://bitbucket.org/team7seniordesign/compareoffer/overview/#markdown-header-install-virtualenv)
5. [Make a virtual environment](https://bitbucket.org/team7seniordesign/compareoffer/overview/#markdown-header-make-a-virtual-environment)
6. [Pull our code](https://bitbucket.org/team7seniordesign/compareoffer/overview/#markdown-header-pull-our-code)
7. [Download our dependencies](https://bitbucket.org/team7seniordesign/compareoffer/overview/#markdown-header-download-our-dependencies)
8. [Start the Django server](https://bitbucket.org/team7seniordesign/compareoffer/overview/#markdown-header-start-the-django-server)

### Install Python 3.3.3
  Python is the language this web application is written in. You'll need to have it installed.

  Head over to the official [Python Download page](http://www.python.org/download/) and grab a release of Python 3.3.3.

  If you'd like, use the graphical installer for an OS X install (which works fine), and then head to [Install setuptools]().

  If you grabbed a tarball, open a terminal and navigate to its download directory (with `cd`). Extract its contents with:

    tar -xavf <tarball>

  This will create a directory `Python-3.3.3/`; `cd` into it. Next, install Python with:

    ./configure && make && sudo make install

### Install setuptools
  Setuptools is a library that assists in Python packaging. We mostly want it for the next step, pip.

  Setuptools can be installed with the following command at a terminal:

    curl "https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py" | sudo python3.3

  **If you're experiencing errors here, you might be missing a system library (zlib I'd bet). Fixing this is a little specific, so contact us and we'll help you out.**

### Install pip
  Pip is a tool that allows for easy installation of Python packages. We will use it to install virtualenv (more on that next), Django (our web application's framework) and other dependencies.

  Pip can be installed with the following:

    curl "https://raw.github.com/pypa/pip/master/contrib/get-pip.py" | sudo python3.3

### Install virtualenv
  *If you do not care about "dirtying" your system install of Python with this project's packages, you may skip to [Pull our code](https://bitbucket.org/team7seniordesign/compareoffer/overview/#markdown-header-pull-our-code), but you will have to prefix any folloing `pip` commands with `sudo`.*

  Virtualenv (*Virtual Env*ironment) allows us to create isolated Python environments. When using a virtualenv, you can install/update/remove/etc python packages and modules without interfering with any other virtualenvs or your "system environment" of Python. This is widely regarded as a good idea.

  Now that we have pip installed, we can use it to grab virtualenv:

    sudo pip install virtualenv

### Make a virtual environment
  We need to make a directory for our isolated virtual environment; it will hold the the packages specific to our project without interfering with your system's packages. I like to put mine at `~/.ve/<project_name>`. Do this with:

    mkdir ~/.ve && virtualenv ~/.ve/compareoffer

  Now that our virtual environment is made, we need to activate it:

    source ~/.ve/compareoffers/bin/activate

  **You will have to activate the virtualenv with the previous command for every new terminal you open.** I know that can sound like a pain, but there are ways to make that less annoying if you intend to do a lot of development on the project.

### Pull our code
  We're almost there! In a terminal where our environment has been activated, run:

    git clone https://bb_account_name@bitbucket.org/team7seniordesign/compareoffer.git

  This step requires a bitbucket.org user account! Replace `bb_account_name` in the previoud command with your own!

  If you'd like to push changes to this codebase, I'd recommend using SSH to connect to bitbucket. You can read more about that [here](https://confluence.atlassian.com/display/BITBUCKET/How+to+install+a+public+key+on+your+Bitbucket+account).

  Then, `cd` into the newly cloned application directory.

### Download our dependencies
  Aside from Python itself, we also use other software to make this site that we must obtain.

  This step is easy with pip:

    pip install -r requirements.txt

### Start the Django server
  Now you just need to create the local database.  This will take a little bit
  as it is populating our cost of living data, but you only have to run it once
  (unless you delete your database file):
    ./manage.py syncdb

  Finally, you can run a server serving our application with the following:

    ./manage.py runserver

  In the output of that command, you will see a local URL (probably [http://127.0.0.1:8000](http://127.0.0.1:8000)). Copy it into your browser and have fun comparing job offers!

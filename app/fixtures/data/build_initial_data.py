import json
import csv
import math
import ipdb

import sys
init_data = []

cols = []
weathers = []

# ipdb.set_trace()
def distance(x1,y1,x2,y2):
    print(x1,y1,x2,y2)
    dist= math.sqrt((x1 - x2)**2 + (y1 - y2)**2)
    return dist

def best_from(l, x,y):
    best = None
    best_dist = 10000000
    i=0
    for c in l:
        # qwer=c
        # ipdb.set_trace()
        x1 = float(x)
        y1 = float(y)
        x2 = float(c["fields"]["lat"])
        y2 = float(c["fields"]["long"])
        this_dist = math.sqrt(((x1-x2)**2) + ((y1-y2)**2))

        if (this_dist < best_dist):
            # print("math.sqrt(((%f-%f)**2) + ((%f-%f)**2)) = %f" % (x1,x2,y1,y2,this_dist))
            # print(i,"best dist", best_dist, "this dist", this_dist)
            i+=1
            best_dist = this_dist
            best = c
    return best


with open("weather.json",'r') as f:
    col_data = json.load(f)
    for i, c in enumerate(col_data):
        datum = {"model":"app.WeatherData"}
        datum["fields"] = c
        datum["pk"] = i+1
        weathers.append(datum)

with open("col.json",'r') as f:
    col_data = json.load(f)
    for i, c in enumerate(col_data):
        datum = {"model":"app.ColData"}
        datum["fields"] = c
        datum["pk"] = i+1
        cols.append(datum)

with open("zipcode.csv",'r') as f:
    reader = csv.DictReader(f)
    for i, r in enumerate(reader):
        r["lat"] = float(r["latitude"])
        r["long"] = float(r["longitude"])

        del r["dst"]
        del r["timezone"]

        del r["longitude"]
        del r["latitude"]

        # print(r["lat"])
        # print(r["long"])

        r["zip_code"] = r["zip"]
        del r["zip"]

        w = best_from(weathers,r["lat"],r["long"])["fields"]
        c = best_from(cols,r["lat"],r["long"])["fields"]

        f_dict = dict(list(r.items()) + list(w.items()) + list(c.items()))

        datum = {"model":"app.Location","fields":f_dict}

        init_data.append(datum)

with open("initial_data.json","w") as f:
    json.dump(init_data, f, indent='\t', sort_keys=True)

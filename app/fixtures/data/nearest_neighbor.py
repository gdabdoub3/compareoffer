# this should not need to be edited
import json
import requests
import pprint
import collections
import datetime

stations = []
session = requests.Session()
weather_objs = []

with open('all_stations.json','r') as f:
    stations = json.load(f)

with open('col.json', 'r') as f:
    cities = json.load(f)
    for city in cities:
        rankings = []
        lat = city["lat"]
        long = city["long"]
        print("Looking for nearest to (%s, %s)" % ( lat, long))

        for station in stations:
            slat = station["latitude"]
            slong = station["longitude"]

            dist = (((slat-lat)**2)+((slong-long)**2))**(0.5)
            station["dist"] = dist

        sorted_stations = sorted(stations, key=lambda s:s["dist"])

        for station in sorted_stations:
            print("\tpossible candidate: %s at (%s, %s)" % (station["name"], station["latitude"], station["longitude"]))

            station_url = "http://www.ncdc.noaa.gov/cdo-web/api/v2/data?limit=100&offset=0&datasetid=normal_mly&datatypeid=mly-tmin-normal&datatypeid=mly-tmax-normal&datatypeid=mly-tavg-normal&datatypeid=mly-prcp-normal&stationid=%s&startdate=%s&enddate=%s" % (station["id"], station["mindate"], station["maxdate"])

            r = session.get(station_url, headers={"token":"0x2a"})

            weather_data = r.json()

            data_by_datatype = collections.defaultdict(list)
            for item in weather_data["results"]:
                k = item["datatype"]
                data_by_datatype[k].append(item)

            weather_data = data_by_datatype["MLY-TAVG-NORMAL"]

            data_by_date = {}
            for item in weather_data:
                k = item["date"]
                date = datetime.datetime.strptime(k[:10], "%Y-%m-%d").strftime("%b").lower()
                data_by_date[date] = float(item["value"])/10

            if not len(weather_data) == 12:
                print("\t\tbad station, moving along")
                continue

            weather_data = data_by_date
            pprint.pprint(weather_data)

            weather_data["lat"]=station["latitude"]
            weather_data["long"]=station["longitude"]


            weather_objs.append(weather_data)
            break

with open("weather.json", "w") as f:
    json.dump(weather_objs, f, indent='\t', sort_keys=True)


$(function() {
  //Bind button listeners
  $('.inline-row-button').click(function() {
    if ($(this).attr("name") === "delete") {
      var offer_id = $(this).attr("value");
      $.ajax({
        url: '/offers/' + offer_id,
        type: 'DELETE',
        success: function(result) {
          // TODO: Should probably be made ajaxy eventually
          location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert("error: " + result);
        }
      });
    }
  }); //end inline-row-button

});

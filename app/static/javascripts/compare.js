
// Javascript for the compare page, offers are accessable via the "offers"
// variable.
var companies_to_compare = {"offers": []};
// On document load.
$(function() {

  $("#tool-box input:checkbox").click(function() {
    if($(this).attr("value")=="map"){
      $("#geo_map").closest("tr").toggle();
    }
    else if($(this).attr("value")=="salary-chart") {
      $("#salary_chart").closest("tr").toggle();
    }
    else if($(this).attr("value")=="col-breakdown") {
      $("#col_pie").closest("tr").toggle();
    }
    else if($(this).attr("value")=="dollar-power") {
      $("#dollar_power").closest("tr").toggle();
    }
    else if($(this).attr("value")=="weather") {
      $("#weather_chart").closest("tr").toggle();
    }
  });

		var total_width = 0;
		$('#companies label').each(function() {
      total_width += $(this).outerWidth( true );
		});
		$("#companies").width(total_width);
   if(total_width>=600){
      $("div #companyDiv").smoothDivScroll({
        mousewheelScrolling: "allDirections"
      });
    }

    var offers = JSON.parse(data_json);
    var user_location = JSON.parse(user_location_json);
    console.log(offers);
    if (offers.offers.length < 1)
      window.location.replace("http://localhost:8000/offers");

    switch(offers.offers.length)
    {
      default:
      case 3:
        //companies_to_compare.offers.push(offers.offers[parseInt(this.id)-1]);
        $("#companies input#3").prop("checked", true);
        $("#companies input#3").parent().addClass("active");
        companies_to_compare.offers.push(offers.offers[2]);
      case 2:
        $("#companies input#2").prop("checked", true);
        $("#companies input#2").parent().addClass("active");
        companies_to_compare.offers.push(offers.offers[1]);
      case 1:
        $("#companies input#1").prop("checked", true);
        $("#companies input#1").parent().addClass("active");
        companies_to_compare.offers.push(offers.offers[0]);
      case 0:
        break;
    }
    render_comparisons(companies_to_compare, path_to_us, user_location);

	$( "#companies input" ).on("change", function() {
		if (!this.checked) {
			var temp_object = offers.offers[jQuery.makeArray(this.parentNode.parentNode.children).indexOf(this.parentNode)];
			companies_to_compare.offers.splice(companies_to_compare.offers.indexOf(temp_object), 1);
		}
		else {
			companies_to_compare.offers.push(offers.offers[jQuery.makeArray(this.parentNode.parentNode.children).indexOf(this.parentNode)]);
		}

    for(var els = document.getElementsByTagName ('svg'), i = els.length; i--;){
      els[i].parentNode.removeChild(els[i]);
    }

    $('#adjust-salary').prop('checked', false);
    render_comparisons(companies_to_compare, path_to_us, user_location);
	});
});


from django import forms
from django.core.exceptions import ValidationError

from app.models import *

from django.contrib.auth.forms import AuthenticationForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import *

# Custom field for company that allows creation of new company.
# class CompanyField(forms.CharField):
    # def clean(self, value):
        # try:
            # # check if exists or create new company
            # company = Company.objects.filter(name=value)
            # if len(company) == 0:
                # company = Company(name=value)
                # company.save()
            # else:
                # company = company[0]
            # return company;
        # except:
            # raise ValidationError

class UserCreateForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'duplicate_email': "A user with that email already exists.",
        'password_mismatch': "The two password fields didn't match.",
        'zip_code_not_exist': "That zip code doesn't seem to exist.",
    }

    states = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]

    email = forms.EmailField(widget=forms.EmailInput(attrs={"placeholder":"Email address"}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"First name"}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Last name"}))
    #zip_code = forms.CharField(max_length=5,widget=forms.TextInput(attrs={"placeholder":"Zip Code"}))
    city = forms.CharField(max_length=50,widget=forms.TextInput(attrs={"placeholder":"City"}))
    state = forms.ChoiceField(choices=[(x, x) for x in states])
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Password"}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Retype Password"}))

    def __init__(self, *args, **kwargs):
        # never show labels or auto ids
        kwargs.pop('auto_id',None)
        super().__init__(auto_id=False,*args, **kwargs)

        self._location = False #we keep this location around so we can use it in clean_zip_code and save with just 1 lookup

        self.helper = FormHelper()
        self.helper.form_id = "signup_form"
        self.helper.form_method = "post"
        self.helper.form_action = "signup"
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Div(
                Fieldset(
                    'Create your account', # first arg is the "legend"/label of fields
                    'email',
                    'first_name',
                    'last_name',
                    'city',
                    'state',
                    'password1',
                    'password2',
                    FormActions(
                        Submit('submit', 'Sign Up', css_class='btn-success'),
                    )
                ),
            css_class = 'col-lg-4 col-md-offset-4',
            )
        )

    class Meta:
        model = User
        fields = ("email","first_name","last_name")

    # def clean_zip_code(self):
    #     zip_code = self.cleaned_data["zip_code"]
    #     try:
    #         self._location = Location._default_manager.get(zip_code=zip_code)
    #         return zip_code
    #     except Location.DoesNotExist:
    #         raise forms.ValidationError(
    #             self.error_messages['zip_code_not_exist'],
    #             code='zip_code_not_exist',
    #         )

    def clean_city(self):
        city = self.cleaned_data["city"]
        try:
            #city = Location._default_manager.filter(city=city, state=state)
            #state = city.filter(state = state)
            #self._location = state[0]
            #self._location = city[0]
            return city
        except Location.DoesNotExist:
            raise forms.ValidationError(
                self.error_messages['zip_code_not_exist'],
                code='zip_code_not_exist',
            )

    def clean_state(self):
        state = self.cleaned_data["state"]
        city = self.cleaned_data["city"]
        try:
            state = Location._default_manager.filter(state=state,city=city)
            self._location = state[0]
            return state
        except Location.DoesNotExist:
            raise forms.ValidationError(
                self.error_messages['zip_code_not_exist'],
                code='zip_code_not_exist',
            )


    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            User._default_manager.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(
            self.error_messages['duplicate_email'],
            code='duplicate_email',
        )

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)

        if self._location:
            user.location = self._location
        else:
            raise ValueError("clean_zip_code should have set self._location")

        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class UserUpdateForm(forms.ModelForm):
    error_messages = UserCreateForm.error_messages

    states = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]


    email = forms.EmailField(widget=forms.EmailInput(attrs={"placeholder":"Email address"}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"First name"}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Last name"}))
    #zip_code = forms.CharField(max_length=5,widget=forms.TextInput(attrs={"placeholder":"Zip Code"}))
    city = forms.CharField(max_length=50,widget=forms.TextInput(attrs={"placeholder":"City"}))
    state = forms.ChoiceField(choices=[(x, x) for x in states])
    password1 = forms.CharField(required=False,widget=forms.PasswordInput(attrs={"placeholder":"Password, if changing"}))
    password2 = forms.CharField(required=False,widget=forms.PasswordInput(attrs={"placeholder":"Retype Password"}))

    class Meta:
        model = User
        fields = ['email','first_name','last_name']

    clean_password2 = UserCreateForm.clean_password2
    #clean_zip_code = UserCreateForm.clean_zip_code

    def __init__(self, *args, **kwargs):
        # never show labels or auto ids
        kwargs.pop('auto_id',None)
        super().__init__(auto_id=False,*args, **kwargs)

        self._location = False

        #self.fields["zip_code"].initial = self.instance.location.zip_code
        self.fields["city"].initial = self.instance.location.city
        self.fields["state"].initial = self.instance.location.state


        self.helper = FormHelper()
        self.helper.form_id = "user_update_form"
        self.helper.form_method = "post"
        self.helper.form_action = "user_update"
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Div(
                Fieldset(
                    'Update your account', # first arg is the "legend"/label of fields
                    'email',
                    'first_name',
                    'last_name',
                    'city',
                    'state',
                    'password1',
                    'password2',
                    FormActions(
                        Submit('submit', 'Update', css_class='btn-success'),
                    )
                ),
            css_class = 'col-lg-4 col-md-offset-4',
            )
        )

    def save(self, commit=True):
        user = super().save(commit=False)
        # only change it if they input something

        if len(self.cleaned_data["password1"]) > 0:
            user.set_password(self.cleaned_data["password1"])

        # if self._location:
        #     user.location = self._location
        # else:
        #     raise ValueError("clean_zip_code should have set self._location")

        try:
            state = self.cleaned_data["state"]
            city = self.cleaned_data["city"]
            user.location = Location.objects.filter(city=city, state=state)[0]
        except:
            raise ValueError("Invalid Location")

        if commit:
            user.save()
        return user


class LoginForm(AuthenticationForm):
    username = forms.EmailField(widget=forms.EmailInput(attrs={"placeholder":"Email address"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Password"}))

    def __init__(self, *args, **kwargs):
        # never show labels or auto ids
        kwargs.pop('auto_id',None)
        super().__init__(auto_id=False,*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = "login_form"
        self.helper.form_method = "post"
        self.helper.form_action = "login"
        self.helper.form_show_labels = False
        self.helper.form_class = 'form-horizontal'
        self.helper.layout = Layout(
            Div(
                Fieldset(
                    'Login', # first arg is the "legend"/label of fields
                    'username',
                    'password',
                    FormActions(
                        Submit('submit', 'Login', css_class='btn-success'),
                    )
                ),
                css_class = 'col-lg-4 col-md-offset-4',
            )
        )

class NavLoginForm(LoginForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = "nav_login_form"
        self.helper.form_method = "post"
        self.helper.form_action = "login"
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Div(
                Field('username'),
                Field('password'),
                FormActions(
                    Submit('submit', 'Login', css_class='btn-success'),
                ),
            ),
        )


class OfferCreateForm(forms.ModelForm):
    error_messages = {
    }
    states = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]

    

    company = forms.CharField(max_length=30);
    #zip_code = forms.CharField(max_length=5,widget=forms.TextInput())
    city = forms.CharField(max_length=50,widget=forms.TextInput(attrs={"placeholder":"City"}))
    state = forms.ChoiceField(choices=[(x, x) for x in states])
    salary =  forms.NumberInput()
    # bootstrap doesn't seem to acknowel
    other_information = forms.CharField(required=False, widget=forms.Textarea())

    class Meta:
        model = Offer
        fields = ['company', 'salary', 'other_information']

    def __init__(self, *args, **kwargs):
        super().__init__(auto_id=False,*args, **kwargs)

        self._location = False

        self.helper = FormHelper()
        self.helper.form_id = "offer_create_form"
        self.helper.form_method = "post"
        self.helper.form_action = "offer_create"
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Div(
                Fieldset(
                    'Record an offer', # first arg is the "legend"/label of fields
                    Field('company', placeholder="Company Name"),
                    Field('salary', placeholder="Salary"),
                    #Field('zip_code', placeholder='Zip Code'),
                    Field('city', placeholder = "City"),
                    Field('state', placeholder = "State"),
                    Field('other_information', placeholder="Additional Information", rows=5),
                    FormActions(
                        Submit('submit', 'Submit', css_class='btn-success'),
                    )
                ),
            css_class = 'col-lg-4 col-md-offset-4',
            )
        )

    # def clean_zip_code(self):
    #     zip_code = self.cleaned_data["zip_code"]
    #     try:
    #         self._location = Location._default_manager.get(zip_code=zip_code)
    #         return zip_code
    #     except Location.DoesNotExist:
    #         raise forms.ValidationError(
    #             self.error_messages['zip_code_not_exist'],
    #             code='zip_code_not_exist',
    #         )
    def clean_city(self):
        city = self.cleaned_data["city"]
        try:
            #city = Location._default_manager.filter(city=city, state=state)
            #state = city.filter(state = state)
            #self._location = state[0]
            #self._location = city[0]
            return city
        except Location.DoesNotExist:
            raise forms.ValidationError(
                self.error_messages['zip_code_not_exist'],
                code='zip_code_not_exist',
            )

    def clean_state(self):
        state = self.cleaned_data["state"]
        city = self.cleaned_data["city"]
        try:
            state = Location._default_manager.filter(state=state,city=city)
            self._location = state[0]
            return state
        except Location.DoesNotExist:
            raise forms.ValidationError(
                self.error_messages['zip_code_not_exist'],
                code='zip_code_not_exist',
            )


    def save(self, commit=True):
        offer = super().save(commit=False)

        if self._location:
            offer.location = self._location
        else:
            raise ValueError("Invalid Location")

        if commit:
            offer.save()
        return offer

class OfferUpdateForm(forms.ModelForm):
    error_messages = {
    }

    states = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]


    company = forms.CharField(max_length=30);
    #zip_code = forms.CharField(max_length=5,widget=forms.TextInput())
    city = forms.CharField(max_length=50,widget=forms.TextInput(attrs={"placeholder":"City"}))
    state = forms.ChoiceField(choices=[(x, x) for x in states])
    salary =  forms.NumberInput()
    # bootstrap doesn't seem to acknowel
    other_information = forms.CharField(required=False, widget=forms.Textarea())

    class Meta:
        model = Offer
        fields = ['company', 'salary', 'other_information']

    def __init__(self, *args, **kwargs):
        super().__init__(auto_id=False,*args, **kwargs)

        #self.fields["zip_code"].initial = self.instance.location.zip_code
        self.fields["city"].initial = self.instance.location.city
        self.fields["state"].initial = self.instance.location.state

        self.helper = FormHelper()
        self.helper.form_id = "offer_update_form"
        self.helper.form_method = "post"
        # self.helper.form_action = "offer_update"
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Div(
                Fieldset(
                    'Update an offer', # first arg is the "legend"/label of fields
                    Field('company', placeholder="Company Name"),
                    Field('salary', placeholder="Salary"),
                    #Field('zip_code', placeholder='Zip Code'),
                    Field('city', placeholder = "City"),
                    Field('state', placeholder = "State"),
                    Field('other_information', placeholder="Additional Information", rows=5),
                    FormActions(
                        Submit('submit', 'Submit', css_class='btn-success'),
                    )
                ),
            css_class = 'col-lg-4 col-md-offset-4',
            )
        )

    class Meta:
        model = Offer
        fields = ['company', 'salary', 'other_information', 'accepted']

    #clean_zip_code = OfferCreateForm.clean_zip_code

    def save(self, commit=True):
        offer = super().save(commit=False)

        # if self._location:
        #     offer.location = self._location
        try:
            state = self.cleaned_data["state"]
            city = self.cleaned_data["city"]
            offer.location = Location.objects.filter(city=city, state=state)[0]
        except:
            raise ValueError("Invalid Location")
        # else:
        #     raise ValueError("clean_zip_code should have set self._location")

        if commit:
            offer.save()
        return offer

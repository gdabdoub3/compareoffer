from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

import json

class Location(models.Model):
    zip_code = models.CharField(null=False, db_index=True, max_length=5, unique=True)

    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255)

    lat = models.FloatField(null=False)
    long = models.FloatField(null=False)

    # avg temperatures, farenheit, by month
    jan = models.FloatField(null=False)
    feb = models.FloatField(null=False)
    mar = models.FloatField(null=False)
    apr = models.FloatField(null=False)
    may = models.FloatField(null=False)
    jun = models.FloatField(null=False)
    jul = models.FloatField(null=False)
    aug = models.FloatField(null=False)
    sep = models.FloatField(null=False)
    oct = models.FloatField(null=False)
    nov = models.FloatField(null=False)
    dec = models.FloatField(null=False)

    # cost of living data
    composite = models.FloatField(null=False)
    grocery = models.FloatField(null=False)
    healthcare = models.FloatField(null=False)
    housing = models.FloatField(null=False)
    transportation = models.FloatField(null=False)
    misc = models.FloatField(null=False)
    utilities = models.FloatField(null=False)

    def __str__(self):
        return "%s, %s %s" % (self.city, self.state, self.zip_code)

    def weather_list(self):
        return [ self.jan, self.feb, self.mar, self.apr, self.may, self.jun,
                self.jul, self.aug, self.sep, self.oct, self.nov, self.dec]

    def col_dict(self):
        return {"overall":self.composite,
                "food":self.grocery,
                "health":self.healthcare,
                "housing": self.housing,
                "transportation":self.transportation,
                "misc":self.misc,
                "utilities":self.utilities}

    def coordinate_list(self):
        return [self.long, self.lat]

    @classmethod
    def dummy_location(cls):
        try:
            location = Location.objects.get(zip_code="00000")
        except Location.DoesNotExist:
            location = Location(city="dummy", state="dummy", lat=0,long=0,
                    jan=0,feb=0,mar=0,apr=0,may=0,jun=0,
                    jul=0,aug=0,sep=0,oct=0,nov=0,dec=0,
                    composite=0,grocery=0,healthcare=0,
                    housing=0,transportation=0,misc=0,utilities=0,
                    zip_code="00000")
            location.save()
        return location


# https://docs.djangoproject.com/en/1.6/topics/auth/customizing/#a-full-example
class UserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password, location):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            location=location,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password, location=None):
        # little hacky, we only run this code in syncdb, before location
        # fixtures have been loaded
        if not location:
            location = Location.dummy_location()

        user = self.create_user(
            email=email,
            first_name=first_name,
            last_name=last_name,
            password=password,
            location=location
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=30, blank=False)

    location = models.ForeignKey(Location, null=False)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        return " ".join((self.first_name, self.lastname))

    def get_short_name(self):
        return self.first_name

    def __str__(self):
        return "%s, %s, %s" % (self.email, self.first_name, self.last_name)

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


# why do we need a company table? we discussed that we wouldn't be doing any
# company-wide stats. isn't this effectively just a CharField that could go
# into Offer? let me know. -Tim
class Company(models.Model):
    name = models.CharField(max_length=50, unique=True)
    location = models.ForeignKey(Location)
    #Derived: Max Salary, Min Salary, Avg Salary, Locations

    def __str__(self):
        return self.name


class Offer(models.Model):
    user = models.ForeignKey(User)
    # company = models.ForeignKey(Company)
    company = models.CharField(max_length=30, blank=False)
    salary = models.DecimalField(max_digits=11,decimal_places = 2)
    other_information = models.CharField(max_length=200, blank=True)
    location = models.ForeignKey(Location)
    accepted = models.BooleanField(default=0)

    def d3_visualization_obj(self):

        col_dict = self.location.col_dict()
        weather_list = self.location.weather_list()
        user_col_dict = self.user.location.col_dict()

        offer_dict = {}
        print("User location: %s" % (self.user.location))
        offer_dict["user_location"] = "%s, %s" % (self.user.location.city, self.user.location.state)
        offer_dict["user_location_dollar_power"] = user_col_dict["overall"]
        offer_dict["company"] = self.company
        offer_dict["city"] = "%s, %s" % (self.location.city, self.location.state)
        offer_dict["coordinates"] = self.location.coordinate_list()
        offer_dict["col"] = col_dict
        offer_dict["weather"] = weather_list
        offer_dict["salary"] = int(self.salary)

        return offer_dict


    def __str__(self):
        return str(self.user) + ": " + str(self.company)


class Preference(models.Model):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=500)

    def __str__(self):
        return self.title


class UserPreference(models.Model):
    user = models.ForeignKey(User)
    preference = models.ForeignKey(Preference)

    def __str__(self):
        return self.user + ": " + self.preference

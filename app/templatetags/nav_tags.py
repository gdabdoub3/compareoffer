from django import template

register = template.Library()

# Tag for determining if current page is active for navigation highlighting.
@register.simple_tag
def navactive(request, pattern):
    # Match this pattern exactly so "Home" isn't highlighted all the time.
    if pattern == "/":
      if request.path == pattern:
        return 'active'
      else:
        return ''
    #if request.path.startswith(pattern):
    if request.path == pattern:
      return 'active'
    return ''

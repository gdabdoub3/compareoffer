from django.core.exceptions import *
from django.http import *
from django.shortcuts import *

from django.core.urlresolvers import reverse_lazy

from django.template.response import TemplateResponse

from django.contrib.auth import login, logout, authenticate, get_user
from django.contrib.auth.views import logout as logout_view, login as login_view
from django.contrib.auth.decorators import login_required

from django.utils.decorators import method_decorator

from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView

from app.models import Offer, User
from app.forms import *

import json
import os

class NavLoginableViewMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['nav_login_form'] = NavLoginForm
        return context

class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class UserCreate(NavLoginableViewMixin, CreateView):
    model = User
    template_name = 'user/create.html'
    form_class=UserCreateForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        super().form_valid(form)

        email = form.cleaned_data["email"]
        password = form.cleaned_data["password1"]

        new_user = authenticate(email=email, password=password)
        login(self.request, new_user)
        return redirect('index') # redirect wasn't reloading the page (with
        # updated nav) sometimes. hopefully render_to_response is better
        # return render_to_response('index.html')


class UserUpdate(LoginRequiredMixin, UpdateView):
    model = User
    form_class=UserUpdateForm
    template_name = 'user/update.html'
    success_url = reverse_lazy('user_update')

    def get_object(self):
        return self.request.user
        # get_user(self.request) might be a good way to do this too


class Index(NavLoginableViewMixin, TemplateView):
    template_name = "index.html"


class About(NavLoginableViewMixin,TemplateView):
    template_name = 'about.html'


############# Offer Views Below ################
class OfferList(LoginRequiredMixin, ListView):
    template_name = "offers/list.html"
    context_object_name = 'offer_list'

    def get_queryset(self):
        offers = Offer.objects.filter(user=get_user(self.request)).order_by("company")
        return offers


class OfferCompare(LoginRequiredMixin, TemplateView):
    template_name = "offers/compare.html"

    def get_context_data(self, **kwargs):
        offers = Offer.objects.filter(user=get_user(self.request))
        d3_offers = [offer.d3_visualization_obj() for offer in offers]
        d3_offers_object = {"offers":d3_offers}

        context = super().get_context_data(**kwargs)

        context["offer_data"] = json.dumps(d3_offers_object)
        context["template_offers"] = offers
        user_location = get_user(self.request).location
        context["user_city"] = user_location.city
        context["user_location"] = json.dumps({"city": user_location.city, "col": user_location.col_dict()})

        return context


class OfferCreate(LoginRequiredMixin, CreateView):
    model = Offer
    template_name = 'offers/create.html'
    form_class=OfferCreateForm

    def form_valid(self, form):
        # need to override to get the current user
        offer = form.save(commit=False)
        offer.user = get_user(self.request)
        offer.save()

        return redirect('offer_list')


class OfferUpdate(LoginRequiredMixin, UpdateView):
    model = Offer
    template_name = 'offers/update.html'
    form_class=OfferUpdateForm
    success_url = reverse_lazy("offer_list")


class OfferDelete(LoginRequiredMixin, DeleteView):
    model = Offer
    success_url = reverse_lazy("offer_list")

from django.conf.urls import patterns, include, url

from django.contrib.auth.decorators import login_required

from django.contrib import admin
admin.autodiscover()

from django.conf import settings

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
)

from app.views import *


urlpatterns += patterns('app.views',
    url(r'^$', Index.as_view(), name="index"),

    url(r'^offers/$', OfferList.as_view(), name="offer_list"),
    url(r'^offers/new/$', OfferCreate.as_view(), name='offer_create'),
    url(r'^offers/(?P<pk>\d+)/delete$', OfferDelete.as_view(), name='offer_delete'),
    url(r'^offers/(?P<pk>\d+)/update$', OfferUpdate.as_view(), name='offer_update'),

    url(r'^offers/compare/$', OfferCompare.as_view(), name='offer_compare'),

    url(r'^about$', About.as_view(), name="about"),
    url(r'^login$', login_view, {'extra_context':{'nav_login_form':NavLoginForm},'template_name': 'login.html', 'authentication_form':LoginForm}, name="login"),
    url(r'^logout$', logout_view, {'next_page': 'index'}, name="logout"),
    url(r'^signup$', UserCreate.as_view(), name='signup'),

    url(r'user/update$', UserUpdate.as_view(), name='user_update'),

)

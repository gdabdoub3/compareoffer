from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponse

# Create your views here.
def index(request):
    context = RequestContext(request, {})
    return render(request, 'index.html', context)

def offer(request, offer_id):
    return HttpResponse("You're looking at offer %s." % offer_id)
